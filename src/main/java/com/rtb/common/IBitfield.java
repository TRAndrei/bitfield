package com.rtb.common;

public interface IBitField extends Comparable<IBitField> {
    void setBit(long position);

    void setBits(long startPosition, long endPosition);

    void clearBit(long position);

    void clearBits(long startPosition, long endPosition);

    void invertBit(long position);

    void invertBits(long startPosition, long endPosition);

    void setValue(long position, long value);

    long getValue(long position, short size);

    long firstSetBit();

    long lastSetBit();
}