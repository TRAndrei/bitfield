package com.rtb.common.concurrent;

public interface IAtomicLongList {

    int size();

    boolean isEmpty();

    int capacity();

    void expand(int capacity);

    void add(long value);

    void insert(int index, long value);

    long get(int index);

    IAtomicLongView getAtomicView(int index);

    long remove(int index);

    void clear();
}
