package com.rtb.common.concurrent.test;

import com.rtb.common.concurrent.AtomicLongList;
import com.rtb.common.concurrent.IAtomicLongList;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;


public class AtomicLongListTest {

    @Test
    public void testCapacity() {

        IAtomicLongList list = new AtomicLongList(10);

        list.expand(15);

        assertEquals(15, list.capacity());
    }
}
