package com.rtb.common.concurrent;

import sun.misc.Unsafe;

import java.lang.reflect.Field;
import java.util.Arrays;

public class AtomicLongList implements IAtomicLongList {

    private static final class AtomicTuple {
        private final long[] array;
        private final boolean inFullArrayOperation;
        private int size;

        private AtomicTuple(long[] array, boolean inFullArrayOperation, int size) {
            this.array = array;
            this.inFullArrayOperation = inFullArrayOperation;
            this.size = size;
        }

        static AtomicTuple makeMarked(AtomicTuple value) {
            return new AtomicTuple(value.array, true, value.size);
        }

        static AtomicTuple makeUnMarked(AtomicTuple value) {
            return new AtomicTuple(value.array, false, value.size);
        }
    }

    private volatile AtomicTuple arrayRef;

    public AtomicLongList(int capacity) {
        long[] array = new long[capacity];

        arrayRef = new AtomicTuple(array, false, 0);
    }

    @Override
    public int size() {
        return arrayRef.size;
    }

    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    @Override
    public int capacity() {
        return arrayRef.array.length;
    }

    @Override
    public void add(long value) {

    }

    @Override
    public void insert(int index, long value) {

    }

    @Override
    public long get(int index) {
        return 0;
    }

    @Override
    public IAtomicLongView getAtomicView(int index) {
        return null;
    }

    @Override
    public long remove(int index) {
        return 0;
    }

    @Override
    public void clear() {

    }

    public void expand(int capacity) {
        // mark the ref so there are no more changes to the data
        AtomicTuple atomicTuple;
        do {
            atomicTuple = arrayRef;

            if (atomicTuple.array.length > capacity) {
                return;
            }
        } while (!compareAndSet(atomicTuple, AtomicTuple.makeMarked(atomicTuple)));

        long[] newArray = new long[capacity];
        System.arraycopy(arrayRef.array, 0, newArray, 0, arrayRef.size);

        arrayRef = new AtomicTuple(newArray, false, arrayRef.size);
    }

    private boolean compareAndSet(AtomicTuple expectedReference,
                                  AtomicTuple newReference) {
        AtomicTuple current = arrayRef;
        return expectedReference == current && ((newReference == current) || casPair(current, newReference));
    }

    // Unsafe mechanics

    private static final sun.misc.Unsafe UNSAFE = getUnsafe();
    private static final long arrayRefOffset =
            objectFieldOffset(UNSAFE, "arrayRef", AtomicLongList.class);

    private static Unsafe getUnsafe() {
        try {
            Field unsafeField = Unsafe.class.getDeclaredField("theUnsafe");
            unsafeField.setAccessible(true);
            return (Unsafe) unsafeField.get(null);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            //
            return null;
        }
    }

    private boolean casPair(AtomicTuple cmp, AtomicTuple val) {
        return UNSAFE.compareAndSwapObject(this, arrayRefOffset, cmp, val);
    }

    static long objectFieldOffset(sun.misc.Unsafe UNSAFE,
                                  String field, Class<?> klazz) {
        try {
            return UNSAFE.objectFieldOffset(klazz.getDeclaredField(field));
        } catch (NoSuchFieldException e) {
            // Convert Exception to corresponding Error
            NoSuchFieldError error = new NoSuchFieldError(field);
            error.initCause(e);
            throw error;
        }
    }
}
