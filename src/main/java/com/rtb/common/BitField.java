package com.rtb.common;

import com.rtb.common.IBitField;

public class BitField implements IBitField {
    @Override
    public void setBit(long position) {

    }

    @Override
    public void setBits(long startPosition, long endPosition) {

    }

    @Override
    public void clearBit(long position) {

    }

    @Override
    public void clearBits(long startPosition, long endPosition) {

    }

    @Override
    public void invertBit(long position) {

    }

    @Override
    public void invertBits(long startPosition, long endPosition) {

    }

    @Override
    public void setValue(long position, long value) {

    }

    @Override
    public long getValue(long position, short size) {
        return 0;
    }

    @Override
    public long firstSetBit() {
        return 0;
    }

    @Override
    public long lastSetBit() {
        return 0;
    }

    @Override
    public int compareTo(IBitField o) {
        return 0;
    }
}
