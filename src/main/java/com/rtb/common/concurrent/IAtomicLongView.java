package com.rtb.common.concurrent;

public interface IAtomicLongView {
    long 	addAndGet(long delta);

    boolean compareAndSet(long expect, long update);

    long 	decrementAndGet();

    long 	get();

    long 	getAndAdd(long delta);

    long 	getAndDecrement();

    long 	getAndIncrement();

    long 	getAndSet(long newValue);

    long 	incrementAndGet();

    void 	lazySet(long newValue);

    long 	longValue();

    void 	set(long newValue);

    String 	toString();
}
